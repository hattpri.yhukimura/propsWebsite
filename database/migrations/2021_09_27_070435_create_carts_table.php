<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('categories_id');
            $table->unsignedBigInteger('subcategories_id');
            $table->unsignedBigInteger('inventories_id');
            $table->unsignedBigInteger('users_id');
            $table->integer('qty')->default('0');
            $table->tinyinteger('status')->default('0');
            $table->dateTime('dateStatus')->nullable();
            $table->timestamps();

            $table->foreign('categories_id')->references('id')
            ->on('categories')
            ->onUpdate('CASCADE');    
            
            $table->foreign('subcategories_id')->references('id')
            ->on('subcategories')
            ->onUpdate('CASCADE');    
            
            $table->foreign('inventories_id')->references('id')
            ->on('inventories')
            ->onUpdate('CASCADE');    
            
            $table->foreign('users_id')->references('id')
            ->on('users')
            ->onUpdate('CASCADE');

        });        
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
