<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCarts2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   
        Schema::table('carts', function (Blueprint $table) {
            $table->string('condition')->nullable()->after('kodeBooking');
            $table->string('statHirer')->nullable()->after('kodeBooking');
            $table->date('dateStarted')->nullable()->after('kodeBooking');
            $table->date('dateReturned')->nullable()->after('kodeBooking');
            $table->tinyInteger('statReturned')->nullable()->default(0)->after('kodeBooking');
            $table->string('conditionReturned')->nullable()->after('kodeBooking');
            $table->integer('amount')->nullable()->default(0)->after('kodeBooking');
            $table->tinyInteger('checkbox')->nullable()->default(0);
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carts', function (Blueprint $table) {
            $table->dropColumn('condition');
            $table->dropColumn('statHirer');
            $table->dropColumn('dateStarted');
            $table->dropColumn('dateReturned');
            $table->dropColumn('statReturned');
            $table->dropColumn('conditionReturned');
            $table->dropColumn('amount');
            $table->dropColumn('checkbox');
        });
    }
}
