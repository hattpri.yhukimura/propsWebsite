<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('categories_id');
            $table->unsignedBigInteger('subcategories_id');
            $table->string('code')->uniqued();
            $table->string('name')->uniqued();
            $table->string('cover')->nullable();
            $table->text('description')->nullable();
            $table->string('condition')->nullable();
            $table->string('status')->nullable();
            $table->bigInteger('daily')->nullable();
            $table->bigInteger('weekly')->nullable();
            $table->bigInteger('monthly')->nullable();
            $table->bigInteger('value')->nullable();
            $table->longText('comments')->nullable();
            $table->string('modify')->nullable();           
            $table->timestamps();

            $table->foreign('categories_id')->references('id')
            ->on('categories')
            ->onUpdate('CASCADE');

            $table->foreign('subcategories_id')->references('id')
            ->on('subcategories')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
