<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBucketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buckets', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('users_id');
            $table->bigInteger('kodeBooking')->unique();
            $table->dateTime('tglBooking')->nullable();
            $table->tinyInteger('statusBooking')->nullable()->default(0);
            $table->timestamps();

            $table->foreign('users_id')->references('id')
            ->on('users')
            ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buckets');
    }
}
