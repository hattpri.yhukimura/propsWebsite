<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superadmin = User::create([
            'name'      => 'Dede Aftafiandi',
            'email'     => 'dede.aftafiandi@test.com',
            'email_verified_at' => date('Y-m-d'),
            'password'  => Hash::make('12345678'),
        ]);

        $superadmin->assignRole('admin');
    }
}
