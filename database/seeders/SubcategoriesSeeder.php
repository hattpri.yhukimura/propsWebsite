<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Subcategories;

class SubcategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subcategories::create([
            'categories_id'         => 1,
            'name_subcategories'    => 'Pistols',
            'imageSubcategories'    => 'https://picsum.photos/200' 
        ]);
    }
}
