<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Inventory;

class InventorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Inventory::create([
            'categories_id'         => 1,
            'subcategories_id'      => 1,
            'code'                  => 'WEAPFA-DUM-PISTOL',
            'name'                  => 'WEAPONNY - PISTOL - DUMMY',
            'cover'                 => 'https://picsum.photos/200',
            'description'           => 'REVOLVER PISTOL - DUMMY - RESIN MOULD',
            'condition'             => 'Very Good',
            'status'                => 'AVAILABLE',
            'daily'                 => 100,
            'weekly'                => 500,
            'monthly'               => 2000,
            'value'                 => 6000,
            'comments'              => '-',
            'modify'                => ' Don\`t Modify'
        ]);
    }
}
