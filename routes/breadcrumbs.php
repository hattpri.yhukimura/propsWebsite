<?php
use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Inventory;

// Weapon Homepage
Breadcrumbs::for('weapon.index', function ($trail) {
    $trail->push('Weapon', route('weapon.index'));
});
Breadcrumbs::for('pistol.index', function ($trail) {
    $trail->push('Weapon', route('weapon.index'));
    $trail->push('pistol', route('pistol.index'));
});

// ADMIN --------------------------------------------------------
Breadcrumbs::for('admin.index', function ($trail) {
    $trail->push('Dashbord', route('admin.index'));
});
// Users 
Breadcrumbs::for('users.index', function ($trail) {
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Users', route('users.index'));
});
// Categoreis
Breadcrumbs::for('categories.index', function ($trail) {
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Category', route('categories.index'));
});
// Categoreis add
Breadcrumbs::for('categories.create', function ($trail) {
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Category', route('categories.index'));
    $trail->push('Add Category', route('categories.create'));
});
// Categoreis edit
Breadcrumbs::for('categories.edit', function ($trail, $categories) {
    $model = Categories::where('name_categories', $categories)->first();
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Category', route('categories.index'));
    $trail->push('Edit Category '.$model->name_categories, route('categories.edit', $categories));
});
// Sub Categoreis
Breadcrumbs::for('sub-categories.index', function ($trail) {
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Sub Category', route('sub-categories.index'));
});
// Sub Categoreis Add
Breadcrumbs::for('sub-categories.create', function ($trail) {
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Sub Category', route('sub-categories.index'));
    $trail->push('Add Sub Category', route('sub-categories.create'));
});
// Sub Categoreis Edit
Breadcrumbs::for('sub-categories.edit', function ($trail, $subcategories) {
    $model = Subcategories::with('categories')->where('name_subcategories', $subcategories)->first();
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Sub Category', route('sub-categories.index'));
    $trail->push('Edit Sub Category '.$model->categories->name_categories.' '.$model->name_subcategories.'', route('sub-categories.edit', $subcategories));
});
// Data Inventories
Breadcrumbs::for('inventories.index', function ($trail) {
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Inventory', route('inventories.index'));
}); 
// Data Inventories Add
Breadcrumbs::for('inventories.create', function ($trail) {
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Inventory', route('inventories.index'));
    $trail->push('Add Inventory', route('inventories.create'));
});
// Data Inventories Edit
Breadcrumbs::for('inventories.edit', function ($trail, $inventory) {
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Inventory', route('inventories.index'));
    $trail->push('Edit Inventory', route('inventories.edit', $inventory));
});
// Data Carts
Breadcrumbs::for('bucket.index', function ($trail) {
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Carts', route('bucket.index'));
}); 
// Data Carts - Show
Breadcrumbs::for('bucket.show', function ($trail, $bucket) {
    $trail->push('Dashbord', route('admin.index'));
    $trail->push('Carts', route('bucket.index'));
    $trail->push('Order : '.$bucket, route('bucket.show', $bucket));
}); 


