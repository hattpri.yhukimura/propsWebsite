<?php
use App\Models\Categories;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Homepage\WeaponController;
use App\Http\Controllers\Homepage\DirectController;
use App\Http\Controllers\Homepage\CartsController;
use App\Http\Controllers\Homepage\Details\PistolController;

// admin
use App\Http\Controllers\Admin\Homepage\AdminHomeController;
use App\Http\Controllers\Admin\Homepage\SubcategoriesController;
use App\Http\Controllers\Admin\Homepage\CategoriesController;
use App\Http\Controllers\Admin\Homepage\InventoriesController;
use App\Http\Controllers\Admin\Homepage\BucketController;
use App\Http\Controllers\Admin\Homepage\UsersController;
use App\Http\Controllers\Admin\DatatablesController;
use App\Http\Controllers\HomeController;

// Livewire





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $categories = Categories::all();
   
    return view('welcome', compact('categories'));
})->name('welcome');


Route::resource('admin/bucket', BucketController::class);

Route::resource('/carts', CartsController::class);

Route::get('admin/users/data', [DatatablesController::class, 'dataUsers'])->name('users.data');
Route::resource('admin/users', UsersController::class);
Route::get('admin/inventories/data', [DatatablesController::class, 'dataInventories'])->name('inventories.data');
Route::resource('admin/inventories', InventoriesController::class);
Route::get('admin/sub-categories/data', [DatatablesController::class, 'dataSubcategories'])->name('subcategories.data');
Route::resource('admin/sub-categories', SubcategoriesController::class);
Route::get('admin/categories/data', [DatatablesController::class, 'dataCategories'])->name('categories.data');
Route::resource('admin/categories', CategoriesController::class);
Route::resource('admin', AdminHomeController::class);

Route::resource('/weapon/pistol', PistolController::class);
Route::resource('/weapon', WeaponController::class);

Auth::routes(['verify' => true]);

Route::get('order-list/code/{id}', [HomeController::class, 'modalDataHistori'])->name('order.modal');
Route::get('order-list/data', [HomeController::class, 'dataHistori'])->name('order.data');
Route::get('order-list', [HomeController::class, 'historical'])->name('order');

Route::put('bucket/unconfirm/{id}', [HomeController::class, 'unconfirmBucket'])->name('unconfirmBucket');
Route::put('bucket/confirm/{id}', [HomeController::class, 'confirmBucket'])->name('confirmBucket');

Route::get('carts/period/update/{id}', [HomeController::class, 'updateTriggerDuration'])->name('updateTriggerDuration');
Route::get('carts/period/modal/{id}', [HomeController::class, 'triggerDuration'])->name('triggerDuration');
Route::get('carts/modals/{id}', [HomeController::class, 'updateTriggerModal'])->name('updateTriggerModal');
Route::get('carts/modal/{id}', [HomeController::class, 'triggerModalCart'])->name('triggerModalCart');
Route::get('home', [HomeController::class, 'index'])->name('home');

Route::get('details/{id}', [DirectController::class, 'details'])->name('details.items');
Route::get('item/{id}', [DirectController::class, 'directDetails'])->name('directDetails');
Route::get('category/{id}', [DirectController::class, 'directItemInventory'])->name('directItemsInventory');
Route::get('directInventory/{id}', [DirectController::class, 'directInventory'])->name('directInventory');
Route::get('{id}', [DirectController::class, 'directSubcategoriesWeapons'])->name('directSubCategories');
Route::get('directHomepage/{id}', [DirectController::class, 'directHomepage'])->name('directHomepage');