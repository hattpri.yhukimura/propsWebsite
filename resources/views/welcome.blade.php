@extends('layouts.default')

@push('styles')
<style>
/* besar dari 601 */
  @media screen and (min-width: 601px) {
    .rFont {
      font-size: 14px;
    }
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 64px;
    }  
    
    .container {
      position: relative;
      width: 100%;
      max-width: 500px;
    }

    .container img {
      width: 100%;
      height: auto;
      max-width: 400px;
      max-height: 250px;
    }

    .container .btn {
      position: absolute;
      top: 65%;
      left: 60%;
      transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      background-color: inherit;
      color: burlywood;
      font-size: 21px;
      padding: 12px 24px;
      border: none;
      cursor: pointer;
      border-radius: 35px;
      text-align: center;
    }

    .container .btn:hover {
      background-color:darkslateblue;
    }

  }
  
  /* kecil dari 600 */
  @media screen and (max-width: 600px) {
    .rFont{
      font-size: 12px;
    }
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 44px;
    }  

    .container {
      position: relative;
      width: 100%;
      max-width: 500px;
    }

    .container img {
      width: 100%;
      height: auto;
      max-width: 400px;
      max-height: 250px;
    }

    .container .btn {
      position: absolute;
      top: 65%;
      left: 60%;
      transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      background-color: inherit;
      color: white;
      font-size: 21px;
      padding: 12px 24px;
      border: none;
      cursor: pointer;
      border-radius: 35px;
      text-align: center;
      display: inline-block;
    }

    .container .btn:hover {
      background-color:darkgrey;
    }
    
  }
  
</style>
@endpush

@section('content')

<div class="row">
  <div class="col">
    <h1 class="text-center mt-3 cursiveFont">PROP HIRE</h1>
  </div>
</div>
<hr>

@include('layouts.partials.alert')

<div class="row rFont justify-content-center">

  @foreach ($categories as $category)
    <div class="col-sm-4 container mt-3">   
      <img src="{{ $category->getCoverParentCategories() }}" alt="{{ $category->name_categories }}">    
      <a href="{{ route('directHomepage', $category->id) }}" class="btn text-uppercase"><b>{{ $category->name_categories }}</b></a> 
    </div>
  @endforeach

</div>

@endsection
