@extends('layouts.default')

@push('styles')
<style>
    @media screen and (min-width: 601px) {
   
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 64px;
    }    
  
  }
  
  /* kecil dari 600 */
  @media screen and (max-width: 600px) {   
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 44px;
    }    
  }
</style>
@endpush

@section('content')

<div class="row justify-content-center">
    <div class="col">
      <h1 class="text-center mt-3 cursiveFont text-uppercase">Prop hire</h1>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-sm-4">
        <div class="card text-white bg-dark mt-3">
            <div class="card-header">{{ __('Confirm Password') }}</div>

                <div class="card-body">
                    {{ __('Please confirm your password before continuing.') }}

                    <form method="POST" action="{{ route('password.confirm') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirm Password') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>

@endsection