@extends('layouts.default')

@push('styles')
<style>
    @media screen and (min-width: 601px) {
   
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 64px;
    }    
  
  }
  
  /* kecil dari 600 */
  @media screen and (max-width: 600px) {   
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 44px;
    }    
  }
</style>
@endpush

@section('content')

<div class="row justify-content-center">
    <div class="col">
      <h1 class="text-center mt-3 cursiveFont text-uppercase">Prop hire</h1>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-sm-4">
        <div class="card text-white bg-dark mt-3">
            <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Send Password Reset Link') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>

@endsection