@extends('layouts.default')

@push('styles')
<style>
    @media screen and (min-width: 601px) {
   
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 64px;
    }    
  
  }
  
  /* kecil dari 600 */
  @media screen and (max-width: 600px) {   
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 44px;
    }    
  }
</style>
@endpush

@section('content')

<div class="row justify-content-center">
    <div class="col">
      <h1 class="text-center mt-3 cursiveFont text-uppercase">Prop hire</h1>
    </div>
</div>

<div class="row justify-content-center">
    <div class="col-sm-4">
        <div class="card text-white bg-dark mt-3">
            <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('click here to request another') }}</button>.
                    </form>
                </div>
        </div>
    </div>
</div>

@endsection