@extends('layouts.default')

@push('styles')
<style>
/* besar dari 601 */
  @media screen and (min-width: 601px) {
    .rFont {
      font-size: 18px;
    }
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 64px;
    }
    .imgSize{
      width: 350px;
      height: 350px;
      max-width: 350px;
      max-height: 350px;
    }
  }
  
  /* kecil dari 600 */
  @media screen and (max-width: 600px) {
    .rFont{
      font-size: 12px;
    }
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 30px;
    }
    .box1{
      max-width: 200px;    
    }
    .imgSize{
      width: 350px;
      height: 350px;
      max-width: 350px;
      max-height: 350px;
    }
  }
  
</style>
@endpush

@push('head')
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>   
@endpush

@section('content')

<div class="row">
    <div class="col">
      <h1 class="text-center mt-3 cursiveFont text-uppercase">{{ $inven->name }}</h1>
    </div>
</div>
<div class="row justify-content-center"> 
  <div class="col-sm-12 text-center">
    <a href="{{ '/' }}" class="btn btn-sm btn-outline-secondary">Props Hire</a>
    <a href="{{ route('directSubCategories', $inven->categories->name_categories) }}" class="btn btn-sm btn-outline-secondary ">{{ $inven->categories->name_categories }}</a>
    <a href="{{ route('directItemsInventory', $inven->subcategories->name_subcategories) }}" class="btn btn-sm btn-outline-secondary">{{ $inven->subcategories->name_subcategories }}</a>  
    <a href="{{ route('pistol.show', 1) }}" class="btn btn-sm btn-outline-secondary active text-uppercase">{{ $inven->code }}</a>    
  </div>
</div>
<hr>
<div class="row justify-content-center"> 
    <div class="col-sm-6">
      <div class="col-sm-12 text-white">   
        <img src="{{ $inven->getCOver() }}" alt="{{ $inven->code }}">
        <h1>{{ $inven->name }}</h1>
        <h2>{{ $inven->description }}</h2>
        <h5 class="float-right">
          @if ($inven->status === "Available")
          <i class='fas fa-circle' style='font-size:30px;color:green'></i>
          @else 
          <i class='fas fa-circle' style='font-size:30px;color:red'></i>
          @endif
           {{ $inven->status ?? '-' }}</h5>
      </div>
    </div>
</div>
@endsection
