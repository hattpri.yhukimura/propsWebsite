@extends('layouts.default')

@push('styles')
<style>
/* besar dari 601 */
  @media screen and (min-width: 601px) {
    .rFont {
      font-size: 14px;
    }
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 64px;
    }
    .imgSize{
      width: 250px;
      height: 250px;
      max-width: 250px;
      max-height: 250px;
    }
  }
  
  /* kecil dari 600 */
  @media screen and (max-width: 600px) {
    .rFont{
      font-size: 12px;
    }
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 30px;
    }
    .box1{
      max-width: 200px;    
    }
    .imgSize{
      width: 250px;
      height: 250px;
      max-width: 250px;
      max-height: 250px;
    }
  }
  
</style>
@endpush

@push('head')
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>   
@endpush

@section('content')

<div class="row">
    <div class="col">
      <h1 class="text-center mt-3 cursiveFont text-uppercase">FireArms - Pistols</h1>
    </div>
</div>

<div class="row justify-content-center"> 
  <div class="col-sm-12 text-center">
    <a href="{{ '/' }}" class="btn btn-sm btn-outline-secondary">Props Hire</a>
    <a href="{{ route('weapon.index') }}" class="btn btn-sm btn-outline-secondary ">Weapons</a>
    <a href="{{ route('pistol.index') }}" class="btn btn-sm btn-outline-secondary active">Pistols</a>    
  </div>
</div>

<hr>

<div class="row rFont justify-content-center">
    <div class="col-sm-2">
      <div class="card bg-dark text-white border-secondary mt-3">
        <img src="https://picsum.photos/200/300" alt="img-category" class="card-img-top imgSize">  
        <div class="card-body">        
          <a href="{{ route('pistol.show', 1) }}" class="btn btn-primary btn-lg card-title text-uppercase">Pistols</a>
        </div>
      </div>
    </div>
    
    @for ($i = 1; $i < 11; $i++)
      <div class="col-sm-2">
        <div class="card bg-dark text-white border-secondary mt-3">
          <img src="https://picsum.photos/200/300" alt="img-category" class="card-img-top imgSize">  
          <div class="card-body">        
            <a href="{{ route('weapon.index') }}" class="btn btn-primary btn-lg card-title text-uppercase">Pistols</a>
          </div>
        </div>
      </div>
    @endfor
    
  </div>
@endsection