@extends('layouts.default')

@push('styles')
<style>
    /* besar dari 601 */
      @media screen and (min-width: 601px) {
        .rFont {
          font-size: 14px;
        }
        .cursiveFont{
          font-family: "Brush Script MT", "cursive";
          font-size: 64px;
        }  
        
        .container {
          position: relative;
          width: 100%;
          max-width: 500px;
        }
    
        .container img {
          width: 100%;
          height: auto;
          max-width: 400px;
          max-height: 250px;
        }
    
        .container .btn {
          position: absolute;
          top: 65%;
          left: 60%;
          transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          background-color: inherit;
          color: burlywood;
          font-size: 21px;
          padding: 12px 24px;
          border: none;
          cursor: pointer;
          border-radius: 35px;
          text-align: center;
        }
    
        .container .btn:hover {
          background-color:darkslateblue;
        }
        .imgSize {
          display: block;
        margin-left: auto;
        margin-right: auto;
        width: 50%;
        height: 150px;
        }
    
      }
      
      /* kecil dari 600 */
      @media screen and (max-width: 600px) {
        .rFont{
          font-size: 12px;
        }
        .cursiveFont{
          font-family: "Brush Script MT", "cursive";
          font-size: 44px;
        }  
    
        .container {
          position: relative;
          width: 100%;
          max-width: 500px;
        }
    
        .container img {
          width: 100%;
          height: auto;
          max-width: 400px;
          max-height: 250px;
        }
    
        .container .btn {
          position: absolute;
          top: 65%;
          left: 60%;
          transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          background-color: inherit;
          color: white;
          font-size: 21px;
          padding: 12px 24px;
          border: none;
          cursor: pointer;
          border-radius: 35px;
          text-align: center;
          display: inline-block;
        }
    
        .container .btn:hover {
          background-color:darkgrey;
        }

        .imgSize {
          display: block;
        margin-left: auto;
        margin-right: auto;
        width: 50%;
        height: 150px;
        }
        
      }

</style>
@endpush

@push('head')
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>   
@endpush

@section('content')

<div class="row">
    <div class="col">
      <h1 class="text-center mt-3 cursiveFont text-uppercase">{{ $sub->categories->name_categories.' - '.$sub->name_subcategories }}</h1>
    </div>
</div>

<div class="row justify-content-center"> 
  <div class="col-sm-12 text-center">
    <a href="{{ '/' }}" class="btn btn-sm btn-outline-secondary">Props Hire</a>
    <a href="{{ route('directSubCategories', $sub->categories->name_categories) }}" class="btn btn-sm btn-outline-secondary ">{{ $sub->categories->name_categories }}</a>
    <a href="{{ route('directItemsInventory', $sub->name_subcategories) }}" class="btn btn-sm btn-outline-secondary active">{{ $sub->name_subcategories }}</a>    
  </div>
</div>

<hr>

<div class="row rFont justify-content-center">
  @foreach ($inven as $item)
  <div class="col-sm-4">
    <div class="card border-dark text-white " style="background: transparent;">
      <img class="card-img-top img-fluid imgSize" src="{{ $item->getCover() }}" alt="{{ $item->code }}">
      <div class="card-body">
        <h5 class="card-title">{{ $item->name }}</h5>        
      </div>
      <div class="card-footer">
        <p class="float-right">
          @if ($item->status === "Available")
              <i class='fas fa-circle' style='font-size:20px;color:green'></i>
          @else 
              <i class='fas fa-circle' style='font-size:20px;color:red'></i>
          @endif
           {{ $item->status ?? '-' }}</p>  
              <a href="{{ route('details.items', $item->code) }}" class="btn btn-primary btn-sm">Detail</a>       
          @if ($item->status === "Available")
            <a href="{{ route('carts.show', $item->id) }}" class="btn btn-sm btn-success">Add Cart</a>
          @endif
      </div>
    </div>
  </div>

  @endforeach

</div>
@endsection