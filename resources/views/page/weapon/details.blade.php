@extends('layouts.default')

@push('styles')
<style>
    /* besar dari 601 */
      @media screen and (min-width: 601px) {
        .rFont {
          font-size: 14px;
        }
        .cursiveFont{
          font-family: "Brush Script MT", "cursive";
          font-size: 64px;
        }  
        
        .container {
          position: relative;
          width: 100%;
          max-width: 500px;
        }
    
        .container img {
          width: 100%;
          height: auto;
          max-width: 400px;
          max-height: 250px;
        }
    
        .container .btn {
          position: absolute;
          top: 65%;
          left: 60%;
          transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          background-color: inherit;
          color: burlywood;
          font-size: 21px;
          padding: 12px 24px;
          border: none;
          cursor: pointer;
          border-radius: 35px;
          text-align: center;
        }
    
        .container .btn:hover {
          background-color:darkslateblue;
        }
        .imgSize {
          display: block;
        margin-left: auto;
        margin-right: auto;
        width: 50%;
        height: 150px;
        }
    
      }
      
      /* kecil dari 600 */
      @media screen and (max-width: 600px) {
        .rFont{
          font-size: 12px;
        }
        .cursiveFont{
          font-family: "Brush Script MT", "cursive";
          font-size: 44px;
        }  
    
        .container {
          position: relative;
          width: 100%;
          max-width: 500px;
        }
    
        .container img {
          width: 100%;
          height: auto;
          max-width: 400px;
          max-height: 250px;
        }
    
        .container .btn {
          position: absolute;
          top: 65%;
          left: 60%;
          transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
          background-color: inherit;
          color: white;
          font-size: 21px;
          padding: 12px 24px;
          border: none;
          cursor: pointer;
          border-radius: 35px;
          text-align: center;
          display: inline-block;
        }
    
        .container .btn:hover {
          background-color:darkgrey;
        }

        .imgSize {
          display: block;
        margin-left: auto;
        margin-right: auto;
        width: 50%;
        height: 150px;
        }
        
      }

</style>
@endpush

@push('head')
<script src='https://kit.fontawesome.com/a076d05399.js' crossorigin='anonymous'></script>   
@endpush

@section('content')

<div class="row">
    <div class="col">
      <h1 class="text-center mt-3 cursiveFont text-uppercase">Details</h1>
    </div>
</div>

<div class="row justify-content-center"> 
  <div class="col-sm-12 text-center">
    <a href="{{ '/' }}" class="btn btn-sm btn-outline-secondary">Props Hire</a>
    <a href="{{ route('directSubCategories', $sub->categories->name_categories) }}" class="btn btn-sm btn-outline-secondary ">{{ $sub->categories->name_categories }}</a>
    <a href="{{ route('directItemsInventory', $sub->subcategories->name_subcategories) }}" class="btn btn-sm btn-outline-secondary ">{{ $sub->subcategories->name_subcategories }}</a>    
    <a href="{{ route('details.items', $sub->id) }}" class="btn btn-sm btn-outline-secondary active">Detail</a>   
  </div>
</div>

<hr>

<div class="row rFont justify-content-center">
  <div class="col-sm-6 text-white">
     
    <div class="card border-dark text-white text-center" style="background: transparent;">
     <center>
       <img class="card-img-top img-fluid" src="{{ $sub->getCover() }}" alt="{{ $sub->code }}" style="width: 50%;">
      </center>
      <div class="card-body">
        <h5 class="card-title">{{ $sub->name }}</h5>   
        @if ($sub->status === "Unavailable")
        <br>
        <h4 class="card-title" style="color: red">Sold Out</h4>
        @else
          <br>
          <h4 class="card-title">Stock : <b>{{ $sub->stock }}</b></h4>
          <a href="{{ route('carts.show', $sub->id) }}" class="btn btn-sm btn-success">Add Cart</a>
        @endif    
      </div>
    </div>
   
    <div class="card-deck mt-3">
      <div class="card border-dark text-white " style="background: transparent;">     
        <div class="card-body">
          <h5 class="card-title">Decription</h5>
          <p class="card-text">{{ $sub->description }}</p>
        </div>      
      </div>

      <div class="card border-dark text-white " style="background: transparent;">     
        <div class="card-body">
          <h5 class="card-title">Code {{ $sub->subcategories->name_subcategories }}</h5>
          <p class="card-text">{{ $sub->code }}</p>
        </div>      
      </div>     

    </div>

    <div class="card-deck mt-3">
      <div class="card border-dark text-white " style="background: transparent;">     
        <div class="card-body">
          <h5 class="card-title">Status</h5>
          <p class="card-text">
            @if ($sub->status === "Unavailable")
                <span style="color: red">Unavailable</span>
            @else
                <span style="color: lightgreen">Available</span>
            @endif
          </p>
        </div>      
      </div>

      <div class="card border-dark text-white " style="background: transparent;">     
        <div class="card-body">
          <h5 class="card-title">Condition</h5>
          <p class="card-text">
            @if ($sub->condition === "fair")
               <span style="color: red">Fair</span>
            @else
            <span style="color: lightgreen">{{ $sub->condition }}</span>
            @endif
          </p>
        </div>      
      </div>     

    </div>



  </div>
</div>
@endsection