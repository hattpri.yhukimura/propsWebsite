@extends('layouts.default')

@push('styles')
<style>
/* besar dari 601 */
.button {
  padding: 15px 25px;
  font-size: 24px;
  text-align: center;
  cursor: pointer;
  outline: none;
  color: #fff;
  background-color: #04AA6D;
  border: none;
  border-radius: 35px;
  box-shadow: 0 9px #999;
}

.button:hover {background-color: #3e8e41}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

  @media screen and (min-width: 601px) {
    .rFont {
      font-size: 14px;
    }
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 64px;
    }  
    
    .container {
      position: relative;
      width: 100%;
      max-width: 500px;
    }

    .container img {
      width: 100%;
      height: auto;
      max-width: 400px;
      max-height: 250px;
    }

    .container .btn {
      position: absolute;
      top: 65%;
      left: 60%;
      transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      background-color: inherit;
      color: burlywood;
      font-size: 21px;
      padding: 12px 24px;
      border: none;
      cursor: pointer;
      border-radius: 35px;
      text-align: center;
    }

    .container .btn:hover {
      background-color:darkslateblue;
    }

  }
  
  /* kecil dari 600 */
  @media screen and (max-width: 600px) {
    .rFont{
      font-size: 12px;
    }
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 44px;
    }  

    .container {
      position: relative;
      width: 100%;
      max-width: 500px;
    }

    .container img {
      width: 100%;
      height: auto;
      max-width: 400px;
      max-height: 250px;
    }

    .container .btn {
      position: absolute;
      top: 65%;
      left: 60%;
      transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      background-color: inherit;
      color: white;
      font-size: 21px;
      padding: 12px 24px;
      border: none;
      cursor: pointer;
      border-radius: 35px;
      text-align: center;
      display: inline-block;
    }

    .container .btn:hover {
      background-color:darkgrey;
    }
    
  }
  
</style>
@endpush

@section('content')

<div class="row">
  <div class="col">
    <h1 class="text-center mt-3 cursiveFont">Carts</h1>
  </div>
</div>
<div class="row justify-content-center"> 
    <div class="col-sm-12 text-center">
      <a href="{{ '/' }}" class="btn btn-sm btn-outline-secondary active">Props Hire</a>   
    </div>
</div>
<hr>
@include('layouts.partials.alert')
@if ($carts->count() !== 0)
<div class="row rFont justify-content-center">
    <div class="col-sm-8">
        <div class="table-responsive table-sm table-condensed">
            <table class="table table-bordered  text-sm  text-white">
                 <thead>
                     <tr>                        
                         <th  class="text-center">Image</th>
                         <th  class="text-center">Category</th>
                         <th  class="text-center">Sub Category</th>
                         <th  class="text-center">Name Item</th>
                         <th  class="text-center">Qty</th>
                         <th colspan="2" class="text-center">period of hire</th>
                         <th  class="text-center">Action</th>
                     </tr>
                 </thead>
                 <tbody> 
                
                     @foreach ($carts as $cart)                      
                       <tr>
                           <td class="text-center"><img src="{{ $cart->inventories->getCover() }}" alt="img" class="img img-fluid img-center" width="250px" height="250px"></td>
                           <td class="text-center">{{ $cart->categories->name_categories }}</td>
                           <td class="text-center">{{ $cart->subcategories->name_subcategories }}</td>
                           <td class="text-center">{{ $cart->inventories->name }}</td>
                           <td class="text-center">{{ $cart->qty }}</td>
                           <td class="text-center">{{ $cart->statHirer ?? 'Set Period'}}</td>
                           <td class="text-center">{{ $cart->amount }}</td>
                           <td>    
                             
                               <a href="{{ route('triggerDuration', $cart->id) }}" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#periodModal" id="period" title="update qty this item">Set Period</a>
                            
                               <a href="{{ route('triggerModalCart', $cart->id) }}" class="btn btn-sm btn-info fas fa-edit" data-toggle="modal" data-target="#showEmployes" id="show" title="update qty this item"></a>

                               <button href="{{ route('carts.destroy', $cart->id) }}" class="btn btn-sm btn-danger fas fa-trash-alt" id="delete"></button>

                           </td>
                       </tr>
                     @endforeach
                 </tbody>   
                 <tfoot>
                     <tr>
                         <td colspan="3"></td>
                         <td  class="text-center text-bold">Total</td>
                         <td  class="text-center  text-bold">{{ $totalQty->sum() }}</td>
                         
                     </tr>
                 </tfoot> 
            </table>           
        </div>
    </div> 
</div>

<div class="row rFont justify-content-center mt-3">
    <button href="{{ route('carts.update', Auth::user()->id) }}" class="btn btn-lg btn-success float-right" style="width: 250px;" id="order">Order</button>
</div>  
@endif


<div class="modal fade bd-example-modal-lg" id="showEmployes" tabindex="-1" role="dialog" aria-labelledby="showEmployesLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" id="contentModal"> 
            
    </div>
 </div>

  <div class="modal fade bd-example-modal-lg" id="periodModal" tabindex="-1" role="dialog" aria-labelledby="showEmployesLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document" id="contentPeriod"> 
            
    </div>
  </div>

 <form action="" method="post" id="deleteForm">
    @csrf
    @method("DELETE")
    <input type="submit" value="Delete" style="display: none">
</form>

<form action="" method="post" id="orderForm">
    @csrf
    @method("PUT")
    <input type="submit" value="Order" style="display: none">
</form>

@endsection

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script>      
    $('a#show').on('click', function(a){        
    	a.preventDefault();
    	var href = $(this).attr('href');   
     
    	$.ajax({
    		url: href,
    		data: 'get',
    		success: function(b){    		
    			$("#contentModal").html(b);    			
    		}
    	});    	

    });  

    $('a#period').on('click', function(a){        
    	a.preventDefault();
    	var href = $(this).attr('href');   
     
    	$.ajax({
    		url: href,
    		data: 'get',
    		success: function(b){    		
    			$("#contentPeriod").html(b);    			
    		}
    	});    	

    });  

    $('button#delete').on('click', function(e){
        e.preventDefault();
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-sm btn-success',
		    cancelButton: 'btn btn-sm btn-danger'
		  },
		  buttonsStyling: false
		});

		Swal.fire({
		  title: 'Are u sure?',		   	  
		  text: "You will not be able to return it",
		  icon: 'warning',		  
		  showCancelButton: true,
		  confirmButtonText: 'Yes, Do it!',
		  cancelButtonText: 'No, Cancel!', 
		}).then((result) => {
		  if (result.value) {
		  	document.getElementById('deleteForm').action = href;
        document.getElementById('deleteForm').submit();

		    swalWithBootstrapButtons.fire(
		      'Deleted!',
		      'Item was successfully removed from cart.',
		      'success'
		    )
		  } else if (
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		      'Canceled',
		      'item was not successfully removed from cart :)',
		      'error'
		    )
		  }
		});        
    });

    $('button#order').on('click', function(e){
        e.preventDefault();
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-sm btn-success',
		    cancelButton: 'btn btn-sm btn-danger'
		  },
		  buttonsStyling: false
		});

		Swal.fire({
		  title: 'Are u sure?',		   	  
		  text: "You will not be able to return it",
		  icon: 'warning',		  
		  showCancelButton: true,
		  confirmButtonText: 'Yes, Do it!',
		  cancelButtonText: 'No, Cancel!', 
		}).then((result) => {
		  if (result.value) {
		  	document.getElementById('orderForm').action = href;
        document.getElementById('orderForm').submit();

		    swalWithBootstrapButtons.fire(
		      'Deleted!',
		      'Item was successfully removed from cart.',
		      'success'
		    )
		  } else if (
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		      'Canceled',
		      'item was not successfully removed from cart :)',
		      'error'
		    )
		  }
		});        
    });

</script>    
@endpush
    
