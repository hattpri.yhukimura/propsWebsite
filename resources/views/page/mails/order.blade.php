<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="row justify-content-center">
    <div class="col">
      <h1 class="text-center mt-3 cursiveFont text-uppercase">Prop hire - Carts Information</h1>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <p>FYI Admin, <br> Someone has entered the carts into the system, please check the admin system to make sure the items exist</p>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">      
            <p>Request By : <b>{{ Auth::user()->name }}</b></p>         
            <table class="table table-sm table-bordered" border="1">
                <thead>
                    <tr>
                        <th class="text-center">Code Item</th>
                        <th class="text-center">Qty</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach ($cart as $item)       
                    <tr>
                        <td >{{ $item->inventories->code }}</td>
                        <td class="text-center">{{ $item->qty }}</td>
                    </tr>
                  @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td class="text-center">Total</td>
                        <td class="text-center">{{ $cart->pluck('qty')->sum() }}</td>
                    </tr>
                </tfoot>
            </table>
       
    </div>
</div>

</body>
</html>