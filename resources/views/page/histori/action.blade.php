<button href="{{ route('order.modal', $id) }}" type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#showEmployes" id="show"><span class="fas fa-eye"></span></button>

<script>
      $('button#show').on('click', function(a){
    	a.preventDefault();
    	var href = $(this).attr('href');    

    	$.ajax({
    		url: href,
    		data: 'get',
    		success: function(b){    		
    			$("#contentModal").html(b);
    		}
    	});    	

    });

</script>