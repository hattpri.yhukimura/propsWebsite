@extends('layouts.default')

@push('styles')
<style>

/* besar dari 601 */
@media screen and (min-width: 601px) {
    .rFont {
      font-size: 14px;
    }
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 64px;
    }  
    
    .container {
      position: relative;
      width: 100%;
      max-width: 500px;
    }

    .container img {
      width: 100%;
      height: auto;
      max-width: 400px;
      max-height: 250px;
    }

    .container .btn {
      position: absolute;
      top: 65%;
      left: 60%;
      transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      background-color: inherit;
      color: burlywood;
      font-size: 21px;
      padding: 12px 24px;
      border: none;
      cursor: pointer;
      border-radius: 35px;
      text-align: center;
    }

    .container .btn:hover {
      background-color:darkslateblue;
    }

  }
  
  /* kecil dari 600 */
  @media screen and (max-width: 600px) {
    .rFont{
      font-size: 12px;
    }
    .cursiveFont{
      font-family: "Brush Script MT", "cursive";
      font-size: 44px;
    }  

    .container {
      position: relative;
      width: 100%;
      max-width: 500px;
    }

    .container img {
      width: 100%;
      height: auto;
      max-width: 400px;
      max-height: 250px;
    }

    .container .btn {
      position: absolute;
      top: 65%;
      left: 60%;
      transform: translate(-50%, -50%);
      -ms-transform: translate(-50%, -50%);
      background-color: inherit;
      color: white;
      font-size: 21px;
      padding: 12px 24px;
      border: none;
      cursor: pointer;
      border-radius: 35px;
      text-align: center;
      display: inline-block;
    }

    .container .btn:hover {
      background-color:darkgrey;
    }
  }
    
  
</style>
@endpush

@push('head')
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}"> 
<link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.css') }}">  
@endpush

@section('content')

<div class="row">
    <div class="col">
      <h1 class="text-center mt-3 cursiveFont text-uppercase">Order</h1>
    </div>
</div>

<div class="row justify-content-center"> 
  <div class="col-sm-12 text-center">
    <a href="{{ '/' }}" class="btn btn-sm btn-outline-secondary">Props Hire</a>    
  </div>
</div>
<hr>
<div class="row justify-content-center">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-body">
        <div class="table-sm table-responsive">
          <table id="order-list" class="table table-striped table-bordered dt-responsive nowrap">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Kode Order</th>
                      <th>Date Order</th>
                      <th>status</th>
                      <th>List Order</th>
                  </tr>
              </thead>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade bd-example-modal-lg" id="showEmployes" tabindex="-1" role="dialog" aria-labelledby="showEmployesLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document" id="contentModal">
    
  </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>



<script>
$(function () {
		$('table#order-list').DataTable({
                retrieve: true,               
		 		processing: true,
                ajax: '{{ route('order.data') }}',
                columns: [
                	{ data: 'DT_RowIndex', orderable: false, searchable : false},
                  { data: 'kodeBooking'},   
                  { data: 'tglBooking'},
                  { data: 'statusBooking'},
                  { data: 'order-button'}
                ],
			    dom: 'Bfrtip',
		        buttons: [
		             'excel'
		        ]                 
		});

	}); 

</script>
    
@endpush