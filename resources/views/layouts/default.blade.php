<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    
    @include('layouts.partials.head')
    @include('layouts.partials.style')    
   
    <body class="hold-transition sidebar-mini layout-fixed"> 
    
      @include('layouts.partials.navbar') 

      <div class="container-fluid" >
        @include('sweetalert::alert')	 
        @yield('content')  
      </div>      
      @include('layouts.partials.jquery')   
      @stack('scripts') 
    </body>
</html>
