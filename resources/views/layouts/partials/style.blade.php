<style>
    body{  
    
      background-repeat: inherit;
      background-size: auto;
      background-image: url({{ asset('storage/assets/img_background.jpg') }});
    }
    .fontFantasy{
      font-family: "Copperplate", "Papyrus", "fantasy";
    }

    @media screen and (min-width: 601px) {
      .imgLogo{
        max-width: 250px;
      }
      .cursiveFont{
        font-family: "Brush Script MT", "cursive";
        font-size: 64px;
        color: aliceblue;
      } 
    }
    @media screen and (max-width: 600px){
      .imgLogo{
        max-width: 100px;
      }
      .cursiveFont{
        font-family: "Brush Script MT", "cursive";
        font-size: 44px;
        color: aliceblue;
      }    
    }

</style>
@stack('styles')