
<nav class="navbar navbar-expand-md navbar-dark bg-dark shadow-sm sticky-top">
  <div class="container-fluid">
    @guest
         <a class="navbar-brand fontFantasy" href="{{ '/' }}">
            <img src="{{ asset('storage/assets/logo_infinite.png') }}" alt="infinite-studios" class="img justify-content-center imgLogo">
         </a>
    @else  
        <a class="navbar-brand fontFantasy" href="{{ '/' }}">
            <img src="{{ asset('storage/assets/logo_infinite.png') }}" alt="infinite-studios" class="img justify-content-center imgLogo">
        </a>
    @endguest
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
          <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <!-- Left Side Of Navbar -->
          <ul class="navbar-nav mr-auto">

          </ul>

          <!-- Right Side Of Navbar -->
          <ul class="navbar-nav ml-auto">
              <!-- Authentication Links -->
                  
              @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('carts.index') }}"><i class="fas fa-shopping-cart"></i> Cart</a>
                    </li>
                     
                  @if (Route::has('login'))
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                      </li>
                  @endif

                  @if (Route::has('register'))
                      <li class="nav-item">
                          <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                      </li>
                  @endif
              @else
                    @role('admin')
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ route('admin.index') }}">{{ __('Administrator') }}</a>
                    </li>
                    @endrole
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('carts.index') }}"><i class="fas fa-shopping-cart"></i> Cart
                            @php
                              $cart = DB::table('carts')->where('users_id', Auth::user()->id)->where('status', 0)->pluck('qty'); 
                            @endphp
                            @if ($cart->sum() !== 0)                             
                                 <b>[ {{ $cart->sum() }} ]</b>                         
                            @endif 
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">{{ __('Profile') }}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('order') }}">{{ __('Order') }}</a>
                    </li>
                  <li class="nav-item dropdown">
                      <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                          {{ Auth::user()->name }}
                      </a>

                      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                          <a class="dropdown-item" href="{{ route('logout') }}"
                             onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
                              {{ __('Logout') }}
                          </a>

                          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                              @csrf
                          </form>
                      </div>
                  </li>
              @endguest
          </ul>
      </div>
  </div>
</nav>