<style>
    body{   
      color: white;
      background-repeat: inherit;
      background-size: auto;
      background-image: url({{ asset('storage/assets/img_background.jpg') }});
    }
    .fontFantasy{
      font-family: "Copperplate", "Papyrus", "fantasy";
    }

    @media screen and (min-width: 601px) {
      .imgLogo{
        max-width: 250px;
      }
    }
    @media screen and (max-width: 600px){
      .imgLogo{
        max-width: 100px;
      }
    }
</style>
@stack('styles')