<footer class="main-footer">
    <strong>Copyright &copy; 2020-{{date('Y')}} Aftafiandi.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> beta
    </div>
</footer>
