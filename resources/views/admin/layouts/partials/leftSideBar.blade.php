 <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <a href="{{ route('admin.index') }}" class="brand-link">     
     <img src="{{ asset('storage/assets/logo_infinite.png') }}" alt="infinite-studios" class="brand-image img-rounded elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light text-sm text-uppercase">Props Admin</span>
    </a>

  
    <div class="sidebar">
      
     <div class="user-panel mt-3 pb-3 mb-3 d-flex"> 
        <div class="info">
          <a href="{{ route('home')}}" class="d-block">{{ Auth::user()->name ?? "??" }}</a>
        </div>
      </div>
    
      <nav class="mt-2">
      <div class="user-panel">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">         
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Management User
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="{{ route('users.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>User</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('admin.index') }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Admin</p>
                    <i class="right fas fa-angle-left"></i>
                  </a>
                </li>
                    
              </ul>                  
          </li>

          <li class="nav-item">
            <a href="{{ route('categories.index') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Category</p>
              <i class="right fas fa-angle-left"></i>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('sub-categories.index') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>sub Category</p>
              <i class="right fas fa-angle-left"></i>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('inventories.index') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Data Inventory</p>
              <i class="right fas fa-angle-left"></i>
            </a>
          </li>

         <li class="nav-item">
            <a href="{{ route('bucket.index') }}" class="nav-link">
              <i class="far fa-circle nav-icon"></i>
              <p>Carts</p>
              <i class="right fas fa-angle-left"></i>
            </a>
          </li>
                    
          

        </ul>
        
      </div>        

      </nav>
     
    </div>
  
  </aside>