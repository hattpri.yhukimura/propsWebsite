<script>
@if(session('success'))
	$.notify({
		// options
		message: '{{ session('success') }}' 
	},{
		// settings
		type: 'success',		
		placement: {
			align: "right",
			from: 'bottom',
		}
	});
@endif
@if(session('info'))
	$.notify({
		// options
		message: '{{ session('info') }}' 
	},{
		// settings
		type: 'info',		
		placement: {
			align: "right",
			from: 'bottom',
		},
	});
@endif
@if(session('danger'))
	$.notify({
		// options
		message: '{{ session('danger') }}' 
	},{
		// settings
		type: 'danger',
		placement: {
			align: "right",
			from: 'bottom'
		}		
	});
@endif
</script>


