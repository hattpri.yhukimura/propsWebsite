@extends('admin.layouts.default')

@push('alerts')

@include('sweetalert::alert')	 

@endpush


@section('content')

<div class="row mb-2">
    <div class="col-sm-12">
        <div class="float-right">
            <a href="{{ route('bucket.create') }}" class="btn btn-sm btn-outline-primary">Add</a>
        </div>
    </div>
</div> 

<div class="row">
   
   @foreach ($buckets as $bucket)
        <div class="col-sm-3">
            <div class="card mb-3">
            <div class="row no-gutters">        
                <div class="col-md-12">
                    <div class="card-body">
                        <h5 class="card-title">{{ $bucket->users->name }}</h5>
                        <p class="card-text text-bold">
                            Code Order : {{ $bucket->kodeBooking }}
                            <br>
                            Date Order : {{ Carbon\Carbon::create($bucket->tglBooking)->toDayDateTimeString() }}
                        </p>
                        <p class="card-text float-right"><small class="text-muted">{{ Carbon\Carbon::create($bucket->tglBooking)->diffForHumans() }}</small></p>
                        <a href="{{ route('bucket.show', $bucket->kodeBooking) }}" class="btn btn-sm btn-outline-dark">Check It</a>
                    </div>
                </div>
            </div>
            </div>
        </div>
   @endforeach

</div>
    
@endsection


@push('jquery')

@endpush