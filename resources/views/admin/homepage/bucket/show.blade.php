@extends('admin.layouts.default')

@push('alerts')

@include('sweetalert::alert')	 

@endpush


@section('content')
<div class="row">
    <div class="col">
       <h5 class="text-bold">Request By : {{ $users->users->name }}</h5>
    </div>
</div>
<div class="row">
  <div class="col-sm-12">
    <table class="table table-bordered table-responsive">
      <thead>
          <tr>
              <th>No</th>
              <th>Item Image</th>
              <th>Name</th>
              <th>Description</th>
              <th>Code</th>              
              <th>Condition</th>
              <th>Availability</th>               
              <th>Current Location</th>            
              <th>Period Of Hire</th>
              <th>Status Order</th>
              <th>Action</th>
             
          </tr>
      </thead>
      <tbody>
          @foreach ($carts as $cart)
              <tr>
                  <td>{{ $no++ }}</td>
                  <td><img src="{{ $cart->inventories->getCover() }}" alt="img" class="img-fluid" width="250px" height="250px"></td>
                  <td>{{ $cart->inventories->name }}</td>
                  <td>{{ $cart->inventories->description }}</td>
                  <td>{{ $cart->inventories->code }}</td>
                  <td>{{ $cart->inventories->condition }} </td>
                  <td>{{ $cart->inventories->status }}</td>
                  <td>{{ $cart->inventories->currentLocation }}</td>
                  <td>{{ $cart->amount }}
                      @if ($cart->statHirer === "Daily")
                          Day
                      @elseif ($cart->statHirer === "Weekly")
                          Week
                      @else 
                          Month
                      @endif
                  </td>
                  <td>
                    @if ($cart->checkbox === 1)
                        Confirmed
                    @else
                        Uncofirmed
                    @endif
                  </td>
                  <td>
                    @if ($cart->checkbox === 1)
                         <button href="{{ route('unconfirmBucket', $cart->id) }}" class="btn btn-sm btn-outline-danger" id="deny" data-toggle="tooltip" data-placement="right" title="Confirmation Item for check out">Deny</button>
                    @else
                         <button href="{{ route('confirmBucket', $cart->id) }}" class="btn btn-sm btn-outline-success" id="delete" data-toggle="tooltip" data-placement="right" title="Confirmation Item for check out">Confirm</button>
                    @endif
                     
                  </td>
                
              </tr>
          @endforeach
      </tbody>       
  </table>
  </div>
</div>

<div class="row justify-content-center">
  {{-- {{ $carts->pluck('kodeBooking')->first() }} --}}
    <button href="{{ route('bucket.update', $carts->pluck('kodeBooking')->first()) }}" class="btn btn-md btn-outline-success mb-5" id="proccess" data-toggle="tooltip" data-placement="right" title="Confirmation Item for check out">Proccess</button>
</div>

<form action="" method="post" id="deleteForm">
    @csrf
    @method("PUT")
    <input type="submit" value="Hapus" style="display: none">
</form>  

<form action="" method="post" id="denyForm">
  @csrf
  @method("PUT")
  <input type="submit" value="Hapus" style="display: none">
</form> 

<form action="" method="post" id="processsForm">
  @csrf
  @method("PUT")
  <input type="submit" value="Hapus" style="display: none">
</form> 

@endsection


@push('jquery')
<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script>
    $('button#delete').on('click', function(e){
        e.preventDefault();
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-sm btn-success',
		    cancelButton: 'btn btn-sm btn-danger'
		  },
		  buttonsStyling: false
		});

		Swal.fire({
		  title: 'Are u sure?',		   	  
		  text: "this need confirm!",
		  icon: 'warning',		  
		  showCancelButton: true,
		  confirmButtonText: 'Yes, Confirm!',
		  cancelButtonText: 'No, Uncofirm!', 
		}).then((result) => {
		  if (result.value) {
		  	document.getElementById('deleteForm').action = href;
            document.getElementById('deleteForm').submit();

		    swalWithBootstrapButtons.fire(
		      'Done!',
		      'Confirmed :)',
		      'success'
		    )
		  } else if (
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		      'Canceled',
		      'Unconfirmed :(',
		      'error'
		    )
		  }
		});  
    });

    $('button#deny').on('click', function(e){
        e.preventDefault();
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-sm btn-success',
		    cancelButton: 'btn btn-sm btn-danger'
		  },
		  buttonsStyling: false
		});

		Swal.fire({
		  title: 'Are u sure?',		   	  
		  text: "cancel this item !!",
		  icon: 'danger',		  
		  showCancelButton: true,
		  confirmButtonText: 'Yes, Confirm!',
		  cancelButtonText: 'No, Unconfirm!', 
		}).then((result) => {
		  if (result.value) {
		  	document.getElementById('denyForm').action = href;
            document.getElementById('denyForm').submit();

		    swalWithBootstrapButtons.fire(
		      'Done!',
		      'Unconfirmed :(',
		      'success'
		    )
		  } else if (
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		      'Canceled',
		      'Okey :)',
		      'error'
		    )
		  }
		});  
    });

    $('button#proccess').on('click', function(e){
        e.preventDefault();
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-sm btn-success',
		    cancelButton: 'btn btn-sm btn-danger'
		  },
		  buttonsStyling: false
		});

		Swal.fire({
		  title: 'Are u sure?',		   	  
		  text: "confirmed status will be sent",
		  icon: 'info',		  
		  showCancelButton: true,
		  confirmButtonText: 'Yes, do it!',
		  cancelButtonText: 'No, dont it!', 
		}).then((result) => {
		  if (result.value) {
		  	document.getElementById('processsForm').action = href;
        document.getElementById('processsForm').submit();

		    swalWithBootstrapButtons.fire(
		      'Done!',
		      'Okey, Order is in process, just a moment :)',
		      'success'
		    )
		  } else if (
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		      'Canceled',
		      'Oops sorry proccess canceled :)',
		      'error'
		    )
		  }
		});  
    });

	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});
</script>
@endpush