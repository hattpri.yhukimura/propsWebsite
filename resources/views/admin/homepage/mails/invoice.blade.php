<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<div class="row justify-content-center">
    <div class="col">
      <h1 class="text-center mt-3 cursiveFont text-uppercase">Prop hire - Carts Information</h1>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <p>Dear {{ $bucket->users->name }}, <br> Thank you for ordering from props hire</p>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">      
            <p>Order Booking : <b>{{ $bucket->kodeBooking }}</b>
                <br>
                Status : <b style="color: lightgreen">Succeed</b>
                <br>
            </p>  
    </div>
</div>
<div class="row">
    <div class="col">
        <p>
            Regard's <br> <b><a href="{{ url('/') }}">Props Hire</a></b>
        </p>
    </div>
</div>

</body>
</html>