@extends('admin.layouts.default')

@push('styles')
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}"> 
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.css') }}">
@endpush


@push('jquery')
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
	{{-- fitur excel --}}
	<script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables/DataTables/JSZip/jszip.min.js') }}"></script>
@endpush

@push('alerts')

@include('sweetalert::alert')	 

@endpush

@section('content')
<div class="row mb-2">
    <div class="col-sm-12">
        <div class="float-right">
            <a href="{{ route('sub-categories.create') }}" class="btn btn-sm btn-outline-primary">Add</a>
        </div>
    </div>
</div> 

<div class="row">
    <div class="col-sm-6">
        <div class="table-responsive table-sm">
            <table id="subcategories" class="table table-bordered table-hover text-sm">
                 <thead>
                     <tr>
                         <th>No</th>
                         <th>Image</th>
                         <th>Category</th>
                         <th>Sub category</th>
                         <th>Action</th>
                     </tr>
                 </thead>
                 <tbody>
              
            </table>
        </div>

    </div>
</div>
<form action="" method="post" id="deleteForm">
    @csrf
    @method("DELETE")
    <input type="submit" value="Hapus" style="display: none">
</form>

@endsection

@push('scripts')

<script>

$(function () {
		$('#subcategories').DataTable({
                retrieve: true,               
		 		processing: true,
                ajax: '{{ route('subcategories.data') }}',
                columns: [
                	{ data: 'DT_RowIndex', orderable: false, searchable : false},
                    { data: 'imageSubcategories', orderable: false, searchable : false}, 
                    { data: 'categories.name_categories'} ,            
                    { data: 'name_subcategories'},
                    { data: 'action', orderable: false, searchable : false}
                ],
			    dom: 'Bfrtip',
		        buttons: [
		             'excel'
		        ]                 
		});
	});


</script>
    
@endpush