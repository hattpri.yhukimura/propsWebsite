@extends('admin.layouts.default')

@push('styles')
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}"> 
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.css') }}">
   
@endpush


@push('jquery')
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
	{{-- fitur excel --}}
	<script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables/DataTables/JSZip/jszip.min.js') }}"></script>
@endpush

@push('alerts')

@include('sweetalert::alert')	 

@endpush

@section('content')
<div class="row mb-2">
    <div class="col-sm-12">
        <div class="float-right">
            <a href="{{ route('inventories.create') }}" class="btn btn-sm btn-outline-primary">Add</a>
        </div>
    </div>
</div> 

<div class="row">
    <div class="col-sm-12">
        <div class="table-sm">
            <table id="subcategories" class="table table-striped table-bordered dt-responsive nowrap">
                 <thead>
                     <tr>
                         <th>No</th>
                         <th>Image</th>
                         <th>Parent Category</th>
                         <th>Sub category</th>
                         <th>Name Item</th>
                         <th>Code</th>
                         <th>Description</th>
                         <th>Condition</th>
                         <th>Status</th>
                         <th>Daily</th>
                         <th>Weekly</th>
                         <th>Monthly</th>
                         <th>Value</th>
                         <th>Note</th>
                         <th>Action</th>
                     </tr>
                 </thead>
            </table>
        </div>
    </div>
</div>
<form action="" method="post" id="deleteForm">
    @csrf
    @method("DELETE")
    <input type="submit" value="Hapus" style="display: none">
</form>

@endsection

@push('scripts')

<script>

$(function () {
		$('#subcategories').DataTable({
                retrieve: true,               
		 		processing: true,
                ajax: '{{ route('inventories.data') }}',
                columns: [
                	{ data: 'DT_RowIndex', orderable: false, searchable : false},
                    { data: 'cover', orderable: false, searchable : false}, 
                    { data: 'categories.name_categories'} ,            
                    { data: 'subcategories.name_subcategories'},
                    { data: 'name'},
                    { data: 'code'},
                    { data: 'description'},
                    { data: 'condition'},
                    { data: 'status'},
                    { data: 'daily'},
                    { data: 'weekly'},
                    { data: 'monthly'},
                    { data: 'value'},
                    { data: 'modify'},
                    { data: 'action', orderable: false, searchable : false}
                ],
			    dom: 'Bfrtip',
		        buttons: [
		             'excel'
		        ]                 
		});
	});


</script>
    
@endpush