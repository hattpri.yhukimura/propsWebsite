@extends('admin.layouts.default')

@push('alerts')

@include('sweetalert::alert')	 

@endpush


@section('content')

<div class="row">
    <div class="col-sm-2">
        <div class="card text-white bg-success">
            <div class="card-header">
                Users
            </div>
            <div class="card-body">
                15 people who have registered
            </div>
            <div class="card-footer float">
                <a href="#" class="btn btn-sm btn-dark">show</a>
            </div>
        </div>
    </div>
</div>
    
@endsection


@push('jquery')

@endpush