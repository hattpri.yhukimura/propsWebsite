<a href="{{ route('categories.edit', $model) }}" class="btn btn-xs btn-warning" data-toggle="tooltip" data-placement="left" title="Edit"><i class='far fa-edit'></i></a>
<button href="{{ route('categories.destroy', $model) }}" class="btn btn-xs btn-danger" id="delete" data-toggle="tooltip" data-placement="right" title="Delete"><i class='fas fa-trash-alt'></i></button>

<link href="//cdn.jsdelivr.net/npm/@sweetalert2/theme-dark@3/dark.css" rel="stylesheet">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script>
	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	});

    $('button#delete').on('click', function(e){
        e.preventDefault();
        var href = $(this).attr('href');

        const swalWithBootstrapButtons = Swal.mixin({
		  customClass: {
		    confirmButton: 'btn btn-sm btn-success',
		    cancelButton: 'btn btn-sm btn-danger'
		  },
		  buttonsStyling: false
		});

		Swal.fire({
		  title: 'Are u sure?',		   	  
		  text: "You will not be able to return it!",
		  icon: 'warning',		  
		  showCancelButton: true,
		  confirmButtonText: 'Yes, do it!',
		  cancelButtonText: 'No, don\'t it!', 
		}).then((result) => {
		  if (result.value) {
		  	document.getElementById('deleteForm').action = href;
            document.getElementById('deleteForm').submit();

		    swalWithBootstrapButtons.fire(
		      'Done!',
		      'Data was successfully deleted from the system.',
		      'success'
		    )
		  } else if (
		    result.dismiss === Swal.DismissReason.cancel
		  ) {
		    swalWithBootstrapButtons.fire(
		      'Canceled',
		      'Data successfully secured :)',
		      'error'
		    )
		  }
		});        
    });
</script>
