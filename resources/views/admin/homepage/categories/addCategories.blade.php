@extends('admin.layouts.default')

@section('content')

<div class="row">
    <div class="col-sm-8">
        <form class="needs-validation" novalidate method="post" action="{{ route('categories.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
              <div class="col-sm-4 mb-3">
                <label for="category">Parent Category</label>
                <input type="text" class="form-control" id="category" placeholder="Category" autocomplete required minlength="3" name="category" value="{{ old('category') }}">
                <div class="valid-feedback">
                  word validation success!
                </div>
              </div>
            </div>
           
            <div class="form-row">
                <div class="col-sm-8 mb-3">
                    <div class="custom-file">
                      <img >
                        <label class="form-label" for="picture">Image for Parent Category <sup>(max: 2mb) (format: jpeg | png | jpg)</sup></label>
                        <br>
                        <img class="img-preview img-fluid" width="250px" height="250px" alt=""> 
                        <input type="file" class="form-control" id="picture" autocomplete required minlength="5" name="picture" value="{{ old('picture') }}" onchange="previewImage()"/>
                    </div>
                    <div class="valid-feedback">
                        image validation success!
                    </div>                  
                </div>
              </div>
            <button class="btn btn-sm btn-outline-primary" type="submit">Add Parent Category</button>
            <a href="{{ route('categories.index') }}" class="btn btn-sm btn-outline-secondary">Back Table Category</a>
          </form>
          
          <script>
          // Example starter JavaScript for disabling form submissions if there are invalid fields
         
          </script>
    </div>    
</div>   
@endsection

@push('scripts')
<script>

function previewImage() {
  const image = document.querySelector('#picture');
  const imgPreview = document.querySelector('.img-preview');

  const oFReader = new FileReader();
  oFReader.readAsDataURL(image.files[0]);

  oFReader.onload = function(oFREvent) {
    imgPreview.src = oFREvent.target.result;
  }
  
}

(function() {
            'use strict';
            window.addEventListener('load', function() {
              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.getElementsByClassName('needs-validation');
              // Loop over them and prevent submission
              var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                  if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                  }
                  form.classList.add('was-validated');
                }, false);
              });
            }, false);
          })();

</script>
@endpush