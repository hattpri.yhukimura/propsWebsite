@extends('admin.layouts.default')

@section('content')

<div class="row">
    <div class="col-sm-8">
        <form class="needs-validation" novalidate method="post" action="{{ route('sub-categories.update', $subcategories->id) }}" enctype="multipart/form-data">
            @csrf
            @method('put')
            <div class="form-row">
              <div class="col-sm-4 mb-3">
                <label for="category">Parent Category</label>
                <select name="category" id="category" class="custom-select select2 form-control " required>
                    @foreach ($categories as $category)
                        <option value="{{ $category->id }}" @if ($category->id === $subcategories->categories_id)
                            selected
                        @endif>{{ $category->name_categories }}</option>
                    @endforeach                    
                </select>
                    <div class="valid-feedback">
                    word validation success!
                    </div>
              </div>
            </div>
            <div class="form-row">
                <div class="col-sm-4 mb-3">
                  <label for="subcategory">Name Sub Category</label>
                  <input type="text" class="form-control" id="subcategory" placeholder="Insert Name For Sub Category" autocomplete required minlength="3" name="subcategory" value="{{ old('subcategory') ?? $subcategories->name_subcategories }}">
                  <div class="valid-feedback">
                    word validation success!
                  </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-sm-8 mb-3">
                    <label for="old_picture">Old Image</label>
                    <br>
                    <img src="{{ $subcategories->getCoverSubcategories() }}" alt="old Image" class="img-fluid" width="150px" height="150px;" id="old_picture">
                </div>
              </div>
            <div class="form-row">
                <div class="col-sm-8 mb-3">
                    <div class="custom-file">
                        <label class="form-label" for="picture">Image for Sub Category <sup>(max: 2mb) (format: jpeg | png | jpg)</sup></label>
                        <input type="file" class="form-control" id="picture" autocomplete minlength="5" name="picture" value="{{ old('picture') }}"/>
                    </div>
                    <div class="valid-feedback">
                        image validation success!
                    </div>
                </div>
              </div>
            <button class="btn btn-sm btn-outline-primary" type="submit">Update Sub Category</button>
            <a href="{{ route('categories.index') }}" class="btn btn-sm btn-outline-secondary">Back Table Sub Category</a>
          </form>
    </div>    
</div>   
@endsection

@push('styles')
	<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
	<script>
		$('.select2').select2({
            placeholder: 'Select Parent Category',
            width: 'resolve',
        });
        
        (function() {
            'use strict';
            window.addEventListener('load', function() {
              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.getElementsByClassName('needs-validation');
              // Loop over them and prevent submission
              var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                  if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                  }
                  form.classList.add('was-validated');
                }, false);
              });
            }, false);
          })();
	</script>
@endpush
