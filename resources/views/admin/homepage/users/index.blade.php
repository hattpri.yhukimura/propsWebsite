@extends('admin.layouts.default')

@push('styles')
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}"> 
	 <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.css') }}">
@endpush


@push('jquery')
	<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
	{{-- fitur excel --}}
	<script src="{{ asset('assets/plugins/datatables-buttons/js/buttons.html5.js') }}"></script>
	<script src="{{ asset('assets/plugins/datatables/DataTables/JSZip/jszip.min.js') }}"></script>
@endpush

@push('alerts')

@include('sweetalert::alert')	 

@endpush


@section('content')

<div class="row">
    <div class="col-sm-6">
        <div class="table-responsive table-sm">
            <table id="categories" class="table table-bordered table-hover text-sm">
                 <thead>
                     <tr>
                         <th>No</th>
                         <th>Name</th>
                         <th>Status</th>
                         <th>Email</th>
                         <th>Created</th>
                         <th>Action</th>
                     </tr>
                 </thead>
                 <tbody>
              
            </table>
        </div>

    </div>
</div>
    
@endsection


@push('jquery')
<script>
    $(function () {
            $('#categories').DataTable({
                    retrieve: true,               
                    processing: true,
                    ajax: '{{ route('users.data') }}',
                    columns: [
                        { data: 'DT_RowIndex', orderable: false, searchable : false},
                        { data: 'name'},              
                        { data: 'role'},
                        { data: 'email'},
                        { data: 'time', orderable: false, searchable: false},
                        { data: 'action', orderable: false, searchable: false},
                    ],
                    dom: 'Bfrtip',
                    buttons: [
                         'excel'
                    ]                 
            });
        });
    </script>
@endpush