@extends('admin.layouts.default')

@section('content')
<div class="row">
    <div class="col-sm-4">
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-sm-8">
        <form class="needs-validation" novalidate method="post" action="{{ route('inventories.store') }}" enctype="multipart/form-data">
            @csrf
            <div class="form-row">
              <div class="col-sm-4 mb-3">
                <label for="category">Category</label>
                <select name="category" id="category" class="custom-select select2 form-control" required autocomplete>
                    <option value="">Choose Category</option>
                        @foreach ($categories as $cate)
                            <optgroup label="{{ $cate->name_categories }}">
                                @foreach ($subcategories->where('categories_id', $cate->id) as $sub)
                                    <option value="{{ $sub->id }}">{{ $sub->name_subcategories }}</option>
                                @endforeach                          
                            </optgroup>
                        @endforeach
                </select>
                <div class="valid-feedback">
                    word validation success!
                </div>
              </div>
              <div class="col-sm-4 mb-3">
                <label for="condition">Condition</label>
                <select name="condition" id="condition" class="custom-select select2 form-control" required autocomplete>
                    <option value="">How is the condition.?</option>
                    <option value="Excellent">Excellent</option> 
                    <option value="Very Good">Very Good</option> 
                    <option value="Good">Good</option>                      
                    <option value="Fair">Fair</option> 
                </select>
                <div class="valid-feedback">
                    word validation success!
                </div>
              </div> 
              <div class="col-sm-4 mb-3">
                    <label for="monthly">Monthly</label>
                    <input type="number" class="form-control @error('monthly') is-invalid @enderror" id="monthly" placeholder="0" min="0" autocomplete required minlength="3" name="monthly" value="{{ old('monthly') }}">
                    <div class="valid-feedback">
                      word validation success!
                    </div>
                </div>             
            </div>
            <div class="form-row">
                <div class="col-sm-4 mb-3">
                  <label for="name">Name Item</label>
                  <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Insert name for item" autocomplete required minlength="3" name="name" value="{{ old('name') }}">
                  <div class="valid-feedback">
                    word validation success!
                  </div>
                </div>
                <div class="col-sm-4 mb-3">
                    <label for="status">Status</label>
                    <select name="status" id="status" class="custom-select select2 form-control" required autocomplete>
                        <option value="">Choose Status Item...</option>
                        <option value="Available">Available</option>
                        <option value="Unavailable">Unavailable</option>                          
                    </select>
                    <div class="valid-feedback">
                        word validation success!
                    </div>
                </div>
                <div class="col-sm-4 mb-3">
                    <label for="value">Value</label>
                    <input type="number" class="form-control @error('value') is-invalid @enderror" id="value" placeholder="0" min="0" autocomplete required minlength="3" name="value" value="{{ old('value') }}">
                    <div class="valid-feedback">
                      word validation success!
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-sm-4 mb-3">
                  <label for="code">Code Item</label>
                  <input type="text" class="form-control @error('code') is-invalid @enderror" id="code" placeholder="Insert code for item" autocomplete required minlength="3" name="code" value="{{ old('code') }}">
                  <div class="valid-feedback">
                    word validation success!
                  </div>
                </div>
                <div class="col-sm-4 mb-3">
                    <label for="daily">Daily</label>
                    <input type="number" class="form-control @error('daily') is-invalid @enderror" id="daily" placeholder="0" min="0" autocomplete required minlength="3" name="daily" value="{{ old('daily') }}">
                    <div class="valid-feedback">
                      word validation success!
                    </div>
                </div>
                <div class="col-sm-4 mb-3">
                    <label for="note">Note</label>
                    <input type="text" class="form-control @error('note') is-invalid @enderror" id="note" placeholder="" name="note" value="{{ old('note') }}">
                    <div class="valid-feedback">
                        word validation success!
                    </div>
                </div>
            </div>
            <div class="form-row">               
                <div class="col-sm-4 mb-3">
                    <label for="weekly">Weekly</label>
                    <input type="number" class="form-control" @error('weekly') is-invalid @enderror id="weekly" placeholder="0" min="0" autocomplete required minlength="3" name="weekly" value="{{ old('weekly') }}">
                    <div class="valid-feedback">
                      word validation success!
                    </div>
                  </div>
             </div>
             <div class="form-row">
                <div class="col-sm-4 mb-3">
                  <label for="description">Description</label>                 
                  <textarea name="description" id="description" cols="30" rows="10" class="form-control @error('description') is-invalid @enderror" placeholder="Insert description item" minlength="5">{{ old('description') }}</textarea>
                  <div class="valid-feedback">
                    word validation success!
                  </div>
                </div>
                <div class="col-sm-4 mb-3">
                    <label for="comment">Comment</label>                 
                    <textarea name="comment" id="comment" cols="30" rows="10" class="form-control @error('comment') is-invalid @enderror" placeholder="Insert comment item">{{ old('comment') }}</textarea>
                    <div class="valid-feedback">
                      word validation success!
                    </div>
                 </div>    
                 <div class="col-sm-4 mb-3">
                    <div class="custom-file">
                        <label class="form-label" for="picture">Image for Item <sup>(max: 2mb) (format: jpeg | png | jpg)</sup></label>
                        <br>
                        <img class="img-preview img-fluid" width="250px" height="250px" alt=""> 
                        <input type="file" class="form-control @error('picture') is-invalid @enderror" id="picture" autocomplete required minlength="5" name="picture" value="{{ old('picture') }}" onchange="previewImage()"/>
                    </div>
                    <div class="valid-feedback">
                        image validation success!
                    </div>
                </div>                           
            </div>          
            <button class="btn btn-sm btn-outline-primary" type="submit">Create Inventory</button>
            <a href="{{ route('categories.index') }}" class="btn btn-sm btn-outline-secondary">Back Table inventory</a>
          </form>
    </div>    
</div>   
@endsection

@push('styles')
	<link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
	<script>
		$('.select2').select2({
            width: 'resolve',
        });

        (function() {
            'use strict';
            window.addEventListener('load', function() {
              // Fetch all the forms we want to apply custom Bootstrap validation styles to
              var forms = document.getElementsByClassName('needs-validation');
              // Loop over them and prevent submission
              var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                  if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                  }
                  form.classList.add('was-validated');
                }, false);
              });
            }, false);
          })();

     function previewImage() {
      const image = document.querySelector('#picture');
      const imgPreview = document.querySelector('.img-preview');

      const oFReader = new FileReader();
      oFReader.readAsDataURL(image.files[0]);

      oFReader.onload = function(oFREvent) {
        imgPreview.src = oFREvent.target.result;
      }
      
    }    
	</script>
@endpush
