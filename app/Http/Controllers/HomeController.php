<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Inventory;
use App\Models\Cart;
use App\Models\Bucket;
use DataTables;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Categories::all();

        return view('home', compact('categories'));
    }

    public function triggerModalCart($id)
    {
        $cart = Cart::with('inventories')->find($id);

        $return   = '
                <div class="modal-dialog modal-dialog-scrollable modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="showEmployesLabel">Cart Detail</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                   
                    <img class="card-img-top img-fluid" src="'.$cart->inventories->getCOver().'" alt="img" tyle="width: 100px; hieght: 100px;">
                  
                    <h4 class="text-center">'.$cart->inventories->name.'</h4>
                    <br>
                    <form action="'.route('updateTriggerModal', $cart->id).'" method="get">
                    <p hidden>'.csrf_token().'</p>
                        <div class="form-row align-items-center justify-content-center">
                            <div class="col-sm-2 my-1">
                                <label class="sr-only" for="name">Name</label>
                                <input type="number" name="qty" class="form-control" id="name" value="'.$cart->qty.'" min="1">
                            
                            </div>
                            <div class="col-sm-2">
                            <button type="submit" class="btn btn-sm btn-info">Update</button>
                            </div>
                        </div>
                    </form>   
                    <br>
                    <p class="text-justify">'.$cart->inventories->description.'</p>
                    <br>
                   
                </div>  
            </div>';

        return $return;

    }

    public function updateTriggerModal(Request $request, $id)
    {
       $cart = Cart::with(['inventories', 'users'])->find($id);

       Cart::where('id', $id)->update([
           'qty' => $request->qty
       ]);

       toast($cart->inventories->name.' has been updated','success');
       
       return redirect()->route('carts.index');
    }

    public function triggerDuration($id)
    {

        $cart = Cart::with('inventories')->find($id);     
 
        $return   = '
                <div class="modal-dialog modal-dialog-scrollable modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="showEmployesLabel"> Set Period Of Hire - '.$cart->inventories->name.'</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                <div class="modal-body">
                    <form action="'.route('updateTriggerDuration', $cart->id).'" method="get">
                    <p hidden>'.csrf_token().'</p>                  
                        <div class="row">
                            <div class="col">
                            <select name="period" id="period" class="form-control">
                                <option value="1">Daily</option>
                                <option value="2">Weekly</option>
                                <option value="3">Monthly</option>
                            </select>
                            </div>
                            <div class="col">
                            <input type="number" name="number" id="number" placeholder="0" min="1" class="form-control" value="'.$cart->amount.'">
                            </div>
                        </div>
                        <div class="row justify-content-center mt-3">
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-sm btn-info">Update</button>
                            </div>
                        </div>
                    </form>                    
                </div>  
            </div>';

        return $return;
    }

    public function updateTriggerDuration(Request $request, $id)
    {
        $cart = Cart::find($id);       

        $period = '';
   

        if ($request->period === "1") {
            $period = 'Daily';
            // $amount = $request->number * $cart->inventories->daily;
        } elseif ($request->period === '2') {
            $period = 'Weekly';
            // $amount = $request->number * $cart->inventories->weekly;
        } elseif ($request->period === '3') {
            $period = 'Monthly';
            // $amount = $request->number * $cart->inventories->monthly;
        }        

        Cart::where('id', $id)->update([
            'statHirer' => $period,
            'amount'    => $request->number,            
        ]);

        toast($cart->inventories->name.' Set Period Of Hire Complate','success');
       
        return redirect()->route('carts.index');

    }

    public function confirmBucket(Request $request, $id)
    {
        $cart = Cart::with('inventories')->where('id', $id)->first();
       
        Cart::where('id', $id)->update([
            'checkbox' => 1,
        ]);

        toast($cart->inventories->name.' Confirmed','success');
        
        return redirect()->route('bucket.show', $cart->kodeBooking);

    }

    public function unconfirmBucket(Request $request, $id)
    {
        $cart = Cart::with('inventories')->where('id', $id)->first();
       
        Cart::where('id', $id)->update([
            'checkbox' => 0,
        ]);

        toast($cart->inventories->name.' Unconfirmed','danger');
        
        return redirect()->route('bucket.show', $cart->kodeBooking);

    }

    public function historical()
    {
        $buckets = Bucket::where('users_id', Auth::user()->id)->get();
        $title = 'Props Hire - Order';

        return view('page.histori.index', compact(['buckets', 'title']));
    }

    public function dataHistori()
    {
        $query = Bucket::where('users_id', Auth::user()->id)->orderBy('tglBooking', 'desc')->get();

       return Datatables::of($query)
       ->addIndexColumn()
       ->editColumn('tglBooking', function(Bucket $bucket){
            $tgl = Carbon::create($bucket->tglBooking);

            return $tgl->toFormattedDateString();
       })
       ->editColumn('statusBooking', function(Bucket $bucket){
           $status = '<p style="color:red;" class="text-bold">On Progress</p>';
           if ($bucket->statusBooking === 1) {
               $status = '<p style="color:green;" class="text-bold">Success</p>';
           }
           return $status;
       })
       ->addColumn('order-button', function(Bucket $bucket){
           return '<a href="'.route('order.modal', $bucket->kodeBooking).'" type="button" class="btn btn-xs btn-info"><span class="fas fa-eye"></span></a>';
       })
       ->rawColumns(['statusBooking', 'order-button'])
       ->toJson();
    }

    public function modalDataHistori($id)
    {
        $title = 'Props Hire - Order List';
        $bucket = Bucket::where('kodeBooking', $id)->first();
        $tgl = Carbon::create($bucket->tglBooking);
        $carts = Cart::with('inventories')->where('kodeBooking', $id)->orderby('kodeBooking', 'desc')->get();

        $no = 1;
        
        return view('page.histori.order-list', compact(['title', 'bucket', 'carts', 'tgl', 'no']));
    }
  
    public function datsse()
    {
        $modal = "
        <div class='modal-dialog modal-dialog-scrollable modal-lg'>
            <div class='modal-content'>
                <div class='modal-header'>
                    <h6 class='modal-title' id='showEmployesLabel'>
                    Order List : <b>$bucket->kodeBooking</b> <br>
                    Date : <b>".$tgl->toFormattedDateString()."</b> <br>
                    Status : $status
                    </h6>
                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'>
                        <span aria-hidden='true'>&times;</span>
                    </button>
                </div>
                <div class='modal-body'>
               
                </div>
            </div>
        </div>";
        return $modal;
    }
    
    
}
