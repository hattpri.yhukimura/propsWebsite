<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

use Spatie\Permission\Traits\HasRoles;

use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Inventory;
use App\Models\User;

use Carbon\Carbon;

class DatatablesController extends Controller
{   
    public function __construct()
    {
        $this->middleware(['auth','verified', 'role:admin']);
    }

    public function dataCategories()
    {
        $query = Categories::all();

        return Datatables::of($query)
        ->addIndexColumn()   
        ->editColumn('imageCategories', function(Categories $categories){
            return '<img src="'.$categories->getCoverParentCategories().'" alt="image_cannot_load" class="img-fluid" height="150px" width=150px;>';
        }) 
        ->addColumn('action', 'admin.homepage.categories.action')
        ->rawColumns(['imageCategories', 'action'])
        ->toJson();
    }

    public function dataSubcategories()
    {
        $query = Subcategories::with('categories')->get();

        return Datatables::of($query)
        ->addIndexColumn()
        ->editColumn('imageSubcategories', function(Subcategories $subcategories){
            return '<img src="'.$subcategories->getCoverSubCategories().'" alt="image_cannot_load" class="img-fluid" height="150px" width=150px;>';
        })
        ->addColumn('action', 'admin.homepage.subcategories.action')
        ->rawColumns(['imageSubcategories', 'action'])
        ->toJson();
    }

    public function dataInventories()
    {
        $query = Inventory::with(['categories', 'subcategories'])->get();

        return Datatables::of($query)
        ->addIndexColumn()
        ->editColumn('cover', function(Inventory $inventory){
            return '<img src="'.$inventory->getCover().'" alt="image_cannot_load" class="img-fluid" height="150px" width=150px;>';
        })       
        ->addColumn('action', 'admin.homepage.inventories.action')
        ->rawColumns(['cover', 'action'])
        ->toJson();
    }

    public function dataUsers()
    {
       $query = User::with('roles')->where('email_verified_at', '!=', Null)->get();

       return Datatables::of($query)
       ->addIndexColumn()
       ->addColumn('action', 'tes')    
       ->addColumn('role', function(User $user){
            $k = $user->roles[0]['name'];
            return $k;
       })
       ->addColumn('time', function(User $user){
            $carbon = Carbon::create($user->email_verified_at);
            return $carbon->toDayDateTimeString() ?? '-';
       })
       ->rawColumns(['action'])
       ->toJson();
    }
    
   
}
