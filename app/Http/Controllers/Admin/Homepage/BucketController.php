<?php

namespace App\Http\Controllers\Admin\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Cart;
use App\Models\Bucket;
use App\Models\Inventory;
use Illuminate\Support\Facades\Mail;
use App\Mail\InvoiceMail;

class BucketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'role:admin']);
    }

    public function index()
    {
       $title = 'Admin - Carts';
       $buckets = Bucket::with('users')->where('statusBooking', 0)->get();     

       return view('admin.homepage.carts', compact(['title', 'buckets']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Admin - Check Carts';
        $carts = Cart::with(['categories', 'subcategories', 'inventories'])->where('kodeBooking', $id)->where('status', 1)->where('statSuccess', 0)->get();
        $users = Cart::with('users')->where('kodeBooking', $id)->first();
        $no = 1;

        return view('admin.homepage.bucket.show', compact(['title', 'carts', 'users', 'no']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Bucket::where('kodeBooking', $id)->update([
            'statusBooking' => 1,
        ]);      

        $bucket = Bucket::with(['users'])->where('kodeBooking', $id)->where('statusBooking', 1)->first();

        $carts = Cart::where('kodeBooking', $bucket->kodeBooking)->where('checkbox', 1)->get();

        foreach ($carts as $cart) {
          Inventory::where('id', $cart->inventories_id)->update([
              'users_id' => $cart->users_id,
              'status' => 'Unavailable'
          ]);

          Cart::where('id', $cart->id)->update([
              'statSuccess' => 1,
          ]);
        }

        Mail::to($bucket->users->email)->send(new InvoiceMail($bucket));

        toast('Cconfirmed item has been delivered successfully','success');

        return redirect()->route('bucket.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
