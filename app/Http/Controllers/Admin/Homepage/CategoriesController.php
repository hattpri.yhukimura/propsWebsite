<?php

namespace App\Http\Controllers\Admin\Homepage;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Models\Categories;

use Intervention\Image\ImageManager;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'role:admin']);
    }
    
    public function index()
    {
        $title = 'Admin - Category';
        return view('admin.homepage.categories', compact(['title']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Admin - Add Category';
        return view('admin.homepage.categories.addCategories', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $validator = Validator::make($request->all(), [
            'category' => 'required|unique:posts|string|max:255',
            'picture' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        
        $cover = null;

        if ($request->hasFile('picture')) {
            $cover =  $request->file('picture')->store('public/assets/parent_categories'); 
        }     

        Categories::create([
            'name_categories' => $request->category,
            'imageCategories' => $cover
        ]);
        toast('Parent Category '.$request->category.' has been created !!','success');        

        return redirect()->route('categories.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {       
        $data = Categories::where('name_categories', $id)->first();
        $title = 'Admin - Edit Category';
        return view('admin.homepage.categories.editCategories', compact(['data', 'title']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Categories::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'category' => 'required|unique:posts|string|min:3',
            'picture' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $cover = null;
     
        if ($request->picture === null) {

            $cover = $category->imageCategories;

        } else {
           
            if ($request->hasFile('picture')) {
                if ($category->imageCategories != 'https://picsum.photos/200') {
                    Storage::delete($category->imageCategories);
                }
                $cover =  $request->file('picture')->store('public/assets/parent_categories'); 
            }  
        }

        Categories::where('id', $category->id)->update([
            'name_categories' => $request->category,
            'imageCategories' => $cover
        ]);

        toast('Parent Category '.$category->name_categories.' has been updated !!','info');

        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        $data = Categories::where('name_categories', $id)->first();   
        Storage::delete($category->imageCategories);
        $category->delete();       

        return redirect()->route('categories.index');
    }
}
