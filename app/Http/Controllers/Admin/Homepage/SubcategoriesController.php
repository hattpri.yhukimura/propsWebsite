<?php

namespace App\Http\Controllers\Admin\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subcategories;
use App\Models\Categories;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class SubcategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'role:admin']);
    }

    public function index()
    {
        $title = 'Admin - Sub Category';
       
        return view('admin.homepage.subcategories', compact(['title']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Admin - Add Sub Category';
        $categories = Categories::all();

        return view('admin.homepage.subcategories.addSubcategories', compact(['title', 'categories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'category'      => 'required',
            'subcategory'   => 'required|unique:post|max:255',
            'picture' => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);
        
        $cover = null;

        if ($request->hasFile('picture')) {
            $cover =  $request->file('picture')->store('public/assets/sub_categories'); 
        }     

        Subcategories::create([
            'categories_id'   => $request->category,
            'name_subcategories' => $request->subcategory,
            'imageSubcategories' => $cover
        ]);
        toast('Sub Category '.$request->subcategory.' has been created !!','success');        

        return redirect()->route('sub-categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategories = Subcategories::where('name_subcategories', $id)->first();
        $categories = Categories::all();
        $title = "Admin - Edit Sub Category";

        return view('admin.homepage.subcategories.editSubcategories', compact(['title', 'subcategories', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subcategories = Subcategories::findOrFail($id);

        $validator = Validator::make($request->all(), [
            'category'      => 'required',
            'subcategory'   => 'required|unique:post|max:255',
            'picture'       => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        if ($request->picture === null) {
            $cover = null;
        } else {
            $cover = $subcategories->imageSubcategories;
        }

        if ($request->hasFile('picture')) {
            Storage::delete($subcategories->imageSubcategories);
            $cover =  $request->file('picture')->store('public/assets/sub_categories'); 
        }     

        Subcategories::where('id', $id)->update([
            'categories_id'   => $request->category,
            'name_subcategories' => $request->subcategory,
            'imageSubcategories' => $cover
        ]);
        toast('Sub Category '.$request->subcategory.' has been Updated !!','success');        

        return redirect()->route('sub-categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $subcategories = Subcategories::where('name_subcategories', $id)->first();
        Storage::delete($subcategories->imageSubcategories);
        $subcategories->delete();

        return redirect()->route('sub-categories.index');
    }
}
