<?php

namespace App\Http\Controllers\Admin\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subcategories;
use App\Models\Categories;
use App\Models\Inventory;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class InventoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified', 'role:admin']);
    }
    
    public function index()
    {
       $title = 'Admin - Inventory';

       return view('admin.homepage.inventories', compact('title'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Admin - Add Inventory';
        $categories = Categories::all();
        $subcategories = Subcategories::all();
        
        return view('admin.homepage.inventories.addInventories', compact(['title', 'categories', 'subcategories']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subcategories = Subcategories::with('categories')->findOrFail($request->category);

       $validator =  Validator::make($request->all(), [
            'category'      => 'required',           
            'name'          => 'required|max:255',
            'code'          => 'required|unique:inventories,code|min:3',
            'picture'       => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            'description'   => 'required|string',
            'condition'     => 'required',
            'status'        => 'required',
            'daily'         => 'required|numeric',
            'weekly'        => 'required|numeric',
            'monthly'       => 'required|numeric',
            'value'         => 'required|numeric',        
        ]);

        $cover = null;
        
        if ($validator->fails()) {
            return redirect()
                        ->route('inventories.create')
                        ->withErrors($validator)
                        ->withInput();
        }else{

            if ($request->hasFile('picture')) {
                $cover =  $request->file('picture')->store('public/assets/inventories'); 
            }   
    
            Inventory::create([
                'categories_id'     => $subcategories->categories->id,
                'subcategories_id'  => $subcategories->id,
                'code'              => $request->code,
                'name'              => $request->name,
                'cover'             => $cover,
                'description'       => $request->description,
                'condition'         => $request->condition,
                'status'            => $request->status,
                'daily'             => $request->daily,
                'weekly'            => $request->weekly,
                'monthly'           => $request->monthly,
                'value'             => $request->value,
                'comments'          => $request->comment,
                'modify'            => $request->note
            ]); 
            
            toast('Success Toast','success');

            return redirect()->route('inventories.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Admin - Update Inventory';
        $categories = Categories::all();
        $subcategories = Subcategories::all();
        $inventory = Inventory::with(['categories', 'subcategories'])->where('code', $id)->first();
        
        return view('admin.homepage.inventories.editInventories', compact(['title', 'categories', 'subcategories', 'inventory']));
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $inventory = Inventory::where('id', $id)->first();      
   
        $subcategories = Subcategories::with('categories')->findOrFail($request->category);
        
        $validator = Validator::make($request->all(), [
            'category'      => 'required',           
            'name'          => 'required|max:255',
            'code'          => 'required|min:3',
            'picture'       => 'file|image|mimes:jpeg,png,jpg|max:2048',
            'description'   => 'required|string',
            'condition'     => 'required',
            'status'        => 'required',
            'daily'         => 'required|numeric',
            'weekly'        => 'required|numeric',
            'monthly'       => 'required|numeric',
            'value'         => 'required|numeric',      
        ]);

        $cover = null;
        if ($validator->fails()) {
            return redirect()
                        ->route('inventories.edit', $id)
                        ->withErrors($validator)
                        ->withInput();
        }else{

            if ($request->picture === null) {
                $cover = $inventory->cover;
            } else {

                if ($request->hasFile('picture')) {
                    Storage::delete($inventory->cover);
                    $cover =  $request->file('picture')->store('public/assets/inventories'); 
                }  
            }

            Inventory::where('id', $inventory->id)->update([
                    'categories_id'     => $subcategories->categories->id,
                    'subcategories_id'  => $subcategories->id,
                    'code'              => $request->code,
                    'name'              => $request->name,
                    'cover'             => $cover,
                    'description'       => $request->description,
                    'condition'         => $request->condition,
                    'status'            => $request->status,
                    'daily'             => $request->daily,
                    'weekly'            => $request->weekly,
                    'monthly'           => $request->monthly,
                    'value'             => $request->value,
                    'comments'          => $request->comment,
                    'modify'            => $request->note
            ]);

            toast('Parent Category '.$inventory->code.' has been updated !!','info');
            return redirect()->route('inventories.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inventory = Inventory::where('code', $id)->first();        
        Storage::delete($inventory->cover);
        $inventory->delete();       

        return redirect()->route('inventories.index');
    }
}
