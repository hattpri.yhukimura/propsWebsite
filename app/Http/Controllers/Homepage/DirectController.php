<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Categories;
use App\Models\Subcategories;
use App\Models\Inventory;

class DirectController extends Controller
{
    public function directHomepage($id)
    {
        $cate = Categories::findOrFail($id);
        $sub = Subcategories::where('categories_id', $cate->id)->value('id');        

        if ($sub !== Null) {
           return redirect()->route('directSubCategories', $cate->name_categories);
        
        }else{
            alert()->error('Oopps Sorry','Page not ready, please contact administrator system');
            return redirect()->back();
        }          

    }

    public function directSubcategoriesWeapons($id)
    {   
       
        $category = Categories::where('name_categories', $id)->first();
       
        $title = "Props Hire - ".$category->name_categories;
        $subcategories = Subcategories::where('categories_id', $category->id)->get();

        return view('page.weapon.homepage', compact(['subcategories', 'category', 'title']));
    }

    public function directInventory($id)
    {
        $sub = Subcategories::findOrFail($id);
        $inven = Inventory::where('subcategories_id', $sub->id)->value('id');

        if ($inven !== Null) {
            return redirect()->route('directItemsInventory', [$sub->name_subcategories]);
        } else {
            alert()->error('Oopps Sorry','Page not ready, please contact administrator system');
            return redirect()->back();
        }
        


    }

    public function directItemInventory($id)
    {
      $sub = Subcategories::with('categories')->where('name_subcategories', $id)->first();
      $inven = Inventory::where('subcategories_id', $sub->id)->get();
      $title = 'Props Hire - '.$sub->name_subcategories;

      return view('page.weapon.items', compact(['title', 'inven', 'sub']));
    }

    public function directDetails($id)
    {
        $inven = Inventory::with(['categories', 'subcategories'])->where('code', $id)->first();
        $title = 'Props Hire - '.$inven->code;

        return view('page.weapon.details.details', compact(['title', 'inven']));
        
    }

    public function details($id)
    {
        $title = 'Props Hire - Details';
        $sub = Inventory::with(['categories', 'subcategories'])->where('code', $id)->first();      

        return view('page.weapon.details', compact(['title', 'sub']));
    }

   
}
