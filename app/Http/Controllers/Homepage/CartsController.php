<?php

namespace App\Http\Controllers\Homepage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\Inventory;
use App\Models\Cart;
use App\Models\User;
use App\Models\Bucket;
use Carbon\Carbon;

use Illuminate\Support\Facades\Mail;
use App\Mail\OrderShipped;


class CartsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }

    public function index()
    {
        $title = 'Props - Carts';
        $carts = Cart::with(['categories','subcategories','inventories'])->where('users_id', Auth::user()->id)->where('status', 0)->orderBy('created_at', 'asc')->get();
        $totalQty = Cart::where('users_id', Auth::user()->id)->where('status', 0)->pluck('qty');
        
        return view('page.carts.index', compact(['title', 'carts', 'totalQty']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Inventory::where('id', $id)->first();
  
        $searchCarts = Cart::where('users_id', Auth::user()->id)->where('inventories_id', $id)->where('status', 0)->first();
      
        if ($searchCarts !== null) {
            Cart::where('id', $searchCarts->id)->update([
                'categories_id'     => $item->categories_id,
                'subcategories_id'  => $item->subcategories_id,
                'inventories_id'    => $item->id,
                'qty'               => $searchCarts->qty + 1,
                'users_id'          => Auth::user()->id,
             ]);

            alert()->success('Add Cart Success','Please check your carts.');

            return redirect()->back();
        } else {

            Cart::create([
                'categories_id' => $item->categories_id,
                'subcategories_id'  => $item->subcategories_id,
                'inventories_id'    => $item->id,
                'qty'               => 1,
                'users_id'          => Auth::user()->id,
                'status'            => 0,
                'dateStatus'        => null
             ]);

            alert()->success('Add Cart Success','Please check your carts.');

            return redirect()->back();
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $carts = Cart::with('inventories')->where('users_id', $id)->where('status', 0)->get();
      
      foreach ($carts as $cartes) {        
        if ($cartes->statHirer === null) {
            toast('Sorry, please specify period of hire for item '.$cartes->inventories->name.'.','warning');
            return redirect()->route('carts.index');
        } 
      }

       $code = date('Ymd').auth::user()->id.rand(100,999);

       foreach ($carts as $cart) {
          Cart::where('id', $cart->id)->update([
              'status'      => 1,
              'kodeBooking' => $code,
              'dateStatus'  => Carbon::now(),
          ]);
       }       
       
       Bucket::create([
           'users_id'       => Auth::user()->id,
           'kodeBooking'    => $code,
           'tglBooking'     => Carbon::now(),
           'statusBooking'  => 0,
       ]);

       Mail::to('dede.aftafiandi@test.com')->send(new OrderShipped($carts));

       toast('Items successfully ordered.','success');
       return redirect()->route('carts.index');
      
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cart = Cart::find($id);
        $cart->delete();
        toast('Item was successfully removed from cart','info');
        return redirect()->route('carts.index');
    }
}
