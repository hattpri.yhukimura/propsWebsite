<?php

namespace App\Http\Livewire\Product;

use Livewire\Component;

use App\Models\Inventory;

class Index extends Component
{
    public function render()
    {
        $products = Inventory::with(['categories', 'subcategories'])->lastest()->get();

        return view('livewire.product.index', compact('products'));
    }
}
