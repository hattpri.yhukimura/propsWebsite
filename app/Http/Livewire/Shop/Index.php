<?php

namespace App\Http\Livewire\Shop;

use Livewire\WithPagination;
use Livewire\Component;
use App\Models\Inventory;

class Index extends Component
{
    use WithPagination;

    public $search;

    protected $updatesQueryString = [
        ['search' => ['except' => '']]
    ];

    public function mount()
    {
        $this->search = request()->query('search', $this->search);
    }

    public function render()
    {  
        
        return view('livewire.shop.index', [
            'products' => $this->search === null ?
            Inventory::latest()->paginate(8) :
            Inventory::latest()->where('name', 'like', '%' . $this->search . '%')->paginate(8)           
        ])->extends('layouts.default');
    }

    public function addToCart($productId)
    {
        $product = Inventory::find($productId);
        Cart::add($product);
        // $this->emit('addToCart');
        dd(Cart::get()['products']);
       
    }

}
