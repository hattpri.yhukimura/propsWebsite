<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'categories_id',
        'subcategories_id',
        'inventories_id',
        'users_id',
        'qty',
        'status',
        'dateStatus',
        'statSuccess',
        'kodeBooking',
        'amount',
        'conditionReturned',
        'statReturned',
        'dateReturned',
        'dateStarted',
        'statHirer',
        'condition',  
        'checkbox'      
    ];

      // protected $guard = [];

    public function categories()
    { 
          return $this->belongsTo(Categories::class);
    }

    public function subcategories()
    { 
          return $this->belongsTo(Subcategories::class);
    }
    
    public function inventories()
    { 
          return $this->belongsTo(Inventory::class);
    }   

    public function users()
    { 
          return $this->belongsTo(User::class);
    } 

}
