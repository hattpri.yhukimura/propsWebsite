<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Subcategories extends Model
{
    use HasFactory;

    protected $fillable = [
        'categories_id',
        'name_subcategories',
        'imageSubcategories'
    ];
    
    public function getCoverSubCategories()
    {
    	if (substr($this->imageSubcategories, 0, 5) == 'https') {
    		return $this->imageSubcategories; 
    	}

    	if ($this->imageSubcategories) {
            $url = Storage::url($this->imageSubcategories);
    		return asset($url);
    	}

    	return asset('storage/assets/erros.png');
    }

    public function categories()
    { 
          return $this->belongsTo(Categories::class);
    }

    public function getRouteKeyName()
    {
        return 'name_subcategories';
    }

}
