<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Storage;

class Categories extends Model
{
    use HasFactory;

    protected $fillable = [
        'name_categories',
        'imageCategories'
    ];


    public function getCoverParentCategories()
    {
    	if (substr($this->imageCategories, 0, 5) == 'https') {
    		return $this->imageCategories; 
    	}

    	if ($this->imageCategories) {
            $url = Storage::url($this->imageCategories);
    		return asset($url);
    	}

    	return asset('storage/assets/erros.png');
    }

    public function getRouteKeyName()
    {
        return 'name_categories';
    }

}
