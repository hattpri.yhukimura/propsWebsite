<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bucket extends Model
{
    use HasFactory;

    protected $fillable = [
        'users_id',
        'kodeBooking',
        'tglBooking',
        'statusBooking',
    ];

    

    public function users()
    { 
        return $this->belongsTo(User::class);
    }

   
}
