<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Inventory extends Model
{
    use HasFactory;

    protected $fillable  = [
          'categories_id',
          'subcategories_id',
          'code',
          'name',
          'cover',
          'description',
          'condition',
          'status',
          'daily',
          'weekly',
          'monthly',
          'value',
          'comment',
          'modify',          
    ];

    public function getCover()
    {
    	if (substr($this->cover, 0, 5) == 'https') {
    		return $this->cover; 
    	}

    	if ($this->cover) {
            $url = Storage::url($this->cover);
    		return asset($url);
    	}

    	return asset('storage/assets/erros.png');
          
    }

    public function categories()
    { 
          return $this->belongsTo(Categories::class);
    }

    public function subcategories()
    { 
          return $this->belongsTo(Subcategories::class);
    }

    public function getRouteKeyName()
    {
        return 'code';
    }
}
